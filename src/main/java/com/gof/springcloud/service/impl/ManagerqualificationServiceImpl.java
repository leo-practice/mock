package com.gof.springcloud.service.impl;

import com.gof.springcloud.entity.Managerqualification;
import com.gof.springcloud.mapper.ManagerqualificationMapper;
import com.gof.springcloud.service.ManagerqualificationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客户经理销售资质信息 服务实现类
 * </p>
 *
 * @author ss
 * @since 2021-05-06
 */
@Service
public class ManagerqualificationServiceImpl extends ServiceImpl<ManagerqualificationMapper, Managerqualification> implements ManagerqualificationService {

}
