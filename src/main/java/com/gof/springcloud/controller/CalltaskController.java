package com.gof.springcloud.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gof.springcloud.entity.Calltask;
import com.gof.springcloud.entity.ResultVo;
import com.gof.springcloud.service.CalltaskService;

import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 呼叫中心任务 前端控制器
 * </p>
 *
 * @author ss
 * @since 2021-05-07
 */
@RestController
@RequestMapping("/calltask")
public class CalltaskController {

	@Autowired
	private CalltaskService calltaskService;

	@PostMapping
	@ApiOperation(value = "Create a calltask")
	public ResultVo<Calltask> createCalltask(@Validated Calltask calltask) {
		calltask.setStatus(0);
		calltaskService.save(calltask);
		ResultVo<Calltask> resultVo = new ResultVo<Calltask>();
		resultVo.success(calltask);
		return resultVo;
	}

	@GetMapping
	@ApiOperation(value = "Get a Calltask info by key")
	public Calltask getCalltask(int key) {
		return calltaskService.getById(key);
	}

	@GetMapping("/query")
	@ApiOperation(value = "query Calltask by status")
	public List<Calltask> get(int status) {
		return calltaskService.list(new QueryWrapper<Calltask>().eq("status", status));
	}

	@PutMapping
	@ApiOperation(value = "Update a calltask")
	public ResultVo<Calltask> updateCalltask(int key) {
		Calltask calltask = calltaskService.getById(key);
		calltask.setStatus(1);
		calltaskService.updateById(calltask);
		ResultVo<Calltask> resultVo = new ResultVo<Calltask>();
		resultVo.success(calltask);
		return resultVo;
	}

}

