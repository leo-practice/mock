package com.gof.springcloud.service;

import com.gof.springcloud.entity.Calltask;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 呼叫中心任务 服务类
 * </p>
 *
 * @author ss
 * @since 2021-05-07
 */
public interface CalltaskService extends IService<Calltask> {

}
