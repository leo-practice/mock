package com.gof.springcloud.service.impl;

import com.gof.springcloud.entity.Client;
import com.gof.springcloud.mapper.ClientMapper;
import com.gof.springcloud.service.ClientService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客户信息 服务实现类
 * </p>
 *
 * @author ss
 * @since 2021-05-06
 */
@Service
public class ClientServiceImpl extends ServiceImpl<ClientMapper, Client> implements ClientService {

}
