package com.gof.springcloud.config;

import java.io.File;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.TemplateConfig;
import com.baomidou.mybatisplus.generator.config.builder.ConfigBuilder;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

public class GeneratorCode {

	public static void main(String args[]) {
		String path = System.getProperty("user.dir") + File.separator + "src"
				+ File.separator + "main" + File.separator + "java";

		DataSourceConfig dsc = new DataSourceConfig();
		dsc.setUrl("jdbc:mysql://47.93.30.94:3306/pe_mock?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC&useSSL=false");
		dsc.setDriverName("com.mysql.jdbc.Driver");
		dsc.setUsername("root");
		dsc.setPassword("vide@lwd523146");
		// 设置包
		PackageConfig pcf = new PackageConfig();
		pcf.setParent("com.gof.springcloud");// 项目名
		TemplateConfig tpl = new TemplateConfig();
		// 全局配置
		GlobalConfig gcf = new GlobalConfig();
		gcf.setFileOverride(true);// 会将旧的文件进行覆盖
		gcf.setBaseColumnList(true);
		gcf.setBaseResultMap(false);
		gcf.setOpen(false);
		gcf.setOutputDir(path);
		gcf.setAuthor("ss");
		gcf.setServiceName("%sService"); // 去掉接口上的I
		gcf.setSwagger2(true); // 实体属性 Swagger2 注解
		gcf.setDateType(DateType.ONLY_DATE);
		// 映射策略
		StrategyConfig sc = new StrategyConfig();
		sc.setRestControllerStyle(true);
		// sc.setTablePrefix("p_");//去掉表前缀
		sc.setInclude(new String[] { "CallTask"});// 指定表名
		sc.setNaming(NamingStrategy.underline_to_camel);// 下划线改为驼峰写法
		// 配置构造器
		ConfigBuilder cb = new ConfigBuilder(pcf, dsc, sc, tpl, gcf);
		// 开始运行
		AutoGenerator generator = new AutoGenerator();
		generator.setConfig(cb);
		generator.execute();
	}
}
