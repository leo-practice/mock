package com.gof.springcloud.service.impl;

import com.gof.springcloud.entity.Calltask;
import com.gof.springcloud.mapper.CalltaskMapper;
import com.gof.springcloud.service.CalltaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 呼叫中心任务 服务实现类
 * </p>
 *
 * @author ss
 * @since 2021-05-07
 */
@Service
public class CalltaskServiceImpl extends ServiceImpl<CalltaskMapper, Calltask> implements CalltaskService {

}
