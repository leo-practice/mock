package com.gof.springcloud.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gof.springcloud.entity.Client;

/**
 * <p>
 * 客户信息 Mapper 接口
 * </p>
 *
 * @author ss
 * @since 2021-05-06
 */
@Mapper
public interface ClientMapper extends BaseMapper<Client> {

}
