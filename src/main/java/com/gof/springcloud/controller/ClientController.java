package com.gof.springcloud.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gof.springcloud.entity.Client;
import com.gof.springcloud.entity.ResultVo;
import com.gof.springcloud.service.ClientService;

import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 客户信息 前端控制器
 * </p>
 *
 * @author ss
 * @since 2021-05-06
 */
@RestController
@RequestMapping("/client")
public class ClientController {
	@Autowired
	private ClientService clientService;

	@PostMapping
	@ApiOperation(value = "Create a client")
	public ResultVo<Client> createClient(@Validated Client client) {
		clientService.save(client);
		ResultVo<Client> resultVo = new ResultVo<Client>();
		resultVo.success(client);
		return resultVo;
	}

	@GetMapping
	@ApiOperation(value = "Get a client info by key")
	public Client getClient(int key) {
		return clientService.getById(key);
	}

}

