package com.gof.springcloud.service;

import com.gof.springcloud.entity.Managerqualification;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 客户经理销售资质信息 服务类
 * </p>
 *
 * @author ss
 * @since 2021-05-06
 */
public interface ManagerqualificationService extends IService<Managerqualification> {

}
