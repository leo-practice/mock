package com.gof.springcloud.service;

import com.gof.springcloud.entity.Client;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 客户信息 服务类
 * </p>
 *
 * @author ss
 * @since 2021-05-06
 */
public interface ClientService extends IService<Client> {

}
