package com.gof.springcloud.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gof.springcloud.entity.Managerqualification;

/**
 * <p>
 * 客户经理销售资质信息 Mapper 接口
 * </p>
 *
 * @author ss
 * @since 2021-05-06
 */
@Mapper
public interface ManagerqualificationMapper extends BaseMapper<Managerqualification> {

}
