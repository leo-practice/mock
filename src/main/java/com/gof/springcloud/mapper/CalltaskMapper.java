package com.gof.springcloud.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gof.springcloud.entity.Calltask;

/**
 * <p>
 * 呼叫中心任务 Mapper 接口
 * </p>
 *
 * @author ss
 * @since 2021-05-07
 */
@Mapper
public interface CalltaskMapper extends BaseMapper<Calltask> {

}
