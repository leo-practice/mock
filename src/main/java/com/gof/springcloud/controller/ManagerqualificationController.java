package com.gof.springcloud.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gof.springcloud.entity.Managerqualification;
import com.gof.springcloud.entity.ResultVo;
import com.gof.springcloud.service.ManagerqualificationService;

import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 客户经理销售资质信息 前端控制器
 * </p>
 *
 * @author ss
 * @since 2021-05-06
 */
@RestController
@RequestMapping("/manager")
public class ManagerqualificationController {
	@Autowired
	private ManagerqualificationService qualificationService;

	@PostMapping
	@ApiOperation(value = "Create a manager qualification")
	public ResultVo<Managerqualification> createManagerqualification(@Validated Managerqualification qualification) {
		qualificationService.save(qualification);
		ResultVo<Managerqualification> resultVo = new ResultVo<Managerqualification>();
		resultVo.success(qualification);
		return resultVo;
	}

	@GetMapping
	@ApiOperation(value = "Get a manager qualification")
	public Managerqualification getQualification(int key) {
		return qualificationService.getById(key);
	}

}

